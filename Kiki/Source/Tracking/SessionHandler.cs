﻿using Kiki.Source.Repository.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiki.Source.Tracking
{
    public class SessionHandler
    {
       private KikiUser activeUser;

       private int sessionID;
       
       /// <summary>
       /// Generate Instance for a User (Create new Session Instance)
       /// </summary>
       /// <param name="user"></param>
       public void CreateUserSession(KikiUser user)
        {
            // Define User
            activeUser = user;

            sessionID = user.Id;
        }

        public KikiUser GetActiveUser()
        {
            return activeUser;
        }

        public int GetSessionID()
        {
            return sessionID;
        }
    }
}
