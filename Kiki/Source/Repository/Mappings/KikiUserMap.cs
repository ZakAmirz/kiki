using FluentNHibernate.Mapping;
using Kiki.Source.Repository.Domains;

namespace Kiki.Source.Repository.Mappings {
    
    
    public class KikiUserMap : ClassMap<KikiUser> {

        public KikiUserMap()
        {
            // ID 
            Id(x => x.Id);

            // Username (Used for Login)
            Map(x => x.Username);

            // Password (Used for Login)
            Map(x => x.Password);

            // Role (Used for Permissions)
            Map(x => x.Role);

            // Name of Table
            Table("kiki_user");

            // Name of Database
            Schema("kiki_software");
        }
    }
}
