﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Kiki.Source.Repository.Mappings;
using NHibernate;

namespace Kiki.Source.Repository
{
    public class NHibernateManager
    {
        private readonly ISessionFactory _dbSessionFactory;

        public NHibernateManager(string connstring)
        {
            _dbSessionFactory = CreateSessionFactory(connstring);
        }

        private ISessionFactory CreateSessionFactory(string connstring)
        {
            return Fluently.Configure()
                      .Database(MySQLConfiguration.Standard.ConnectionString(connstring))
                      .Cache(c => c.UseQueryCache().UseSecondLevelCache().UseMinimalPuts())
                      .Mappings(m => m.FluentMappings.Add<KikiUserMap>())
                      .BuildSessionFactory();
        }

        public ISessionFactory GetSessionFactory()
        {
            return _dbSessionFactory;
        }
    }
}