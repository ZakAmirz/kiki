using System;
using System.Text;
using System.Collections.Generic;


namespace Kiki.Source.Repository.Domains {

    public class KikiUser
    {
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual int Role { get; set; }
    }
}
