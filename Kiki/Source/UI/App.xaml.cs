﻿using Kiki.Source.Repository;
using Kiki.Source.Tracking;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Kiki
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static NHibernateManager databaseManager;

        public static SessionHandler currentSession;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MySqlConnectionStringBuilder connectionString = new MySqlConnectionStringBuilder
            {
                Server = "127.0.0.1",
                UserID = "kiki",
                Password = "testing",
                Database = "kiki_software",
            };

            databaseManager = new NHibernateManager(connectionString.ToString());

            currentSession = new SessionHandler();
        }
    }
}
