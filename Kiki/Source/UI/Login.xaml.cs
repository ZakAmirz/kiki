﻿using Kiki.Source.Repository.Domains;
using Kiki.Source.UI;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kiki
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml 
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            MouseDown += Window_MouseDown;
        }

        /// <summary>
        /// Allows the user to drag the window freely, by left clicking the application.
        /// </summary>
        /// <param name="sender">The user</param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        /// <summary>
        /// When user clicks exit on the File > Exit, the program will shutdown.
        /// </summary>
        /// <param name="sender">The user</param>
        /// <param name="e"></param>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// The event is executed when the Login Button is clicked.
        /// </summary>
        /// <param name="sender">The user</param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            // If both the username and password fields are empty, give a message asking to enter into both fields.
            if (username_Box.Text.Equals("") && password_Box.Password.Equals(""))
            {
                MessageBox.Show("Please enter a username and password!");
            }

            // If username field is only empty, give a message to enter username.
            else if (username_Box.Text.Equals(""))
            {
                MessageBox.Show("Please enter a username!");
            }

            // If password field is only empty, give a message to enter username.
            else if (password_Box.Password.Equals(""))
            {
                MessageBox.Show("Please enter a password!");
            }

            //TODO: Database Logic using Entity Framework 6 + Login Logic for Authenticating a user with the database.

            // Passed Data Checks
            else
            {
                // Testing if we mapped the database correctly and everything works! (Please work...)
                using (ISession activeSession = App.databaseManager.GetSessionFactory().OpenSession())
                {
                    // Define user
                    KikiUser user;

                    // Get user by the username entered in the GUI
                    user = activeSession.CreateCriteria<KikiUser>().Add(Restrictions.Eq("Username", username_Box.Text)).UniqueResult<KikiUser>();

                    // Username does not exist
                    if (user == null)
                    {
                        MessageBox.Show("Username does not exist!");
                    }

                    // Password do not match
                    else if (user.Password != password_Box.Password)
                    {
                        MessageBox.Show("Sorry but that password is incorrect!");

                    }

                    // Passwords match
                    else if (user.Password == password_Box.Password)
                    {
                        // Create New Session for User
                        App.currentSession.CreateUserSession(user);

                        //Login was successful
                        MessageBox.Show("You have logged in successfully! With Session ID " + App.currentSession.GetActiveUser().Id);

                        MainMenu newMenu = new MainMenu();

                        newMenu.Show();
                        this.Close();
                    }
                }
            }
        }
    }
}
